"use client"
import { useState } from "react";
import ModalCategories from "../Modal/ModalCategories";
import { IoFilterSharp } from "react-icons/io5";
import Image from "next/image";

const NavBarFiltre = () => {
    const [activeModal,setActiveModal] = useState(false);
    const toggleModal = () =>{
        setActiveModal(!activeModal);
    }
    return (
        <>
            <ModalCategories active={activeModal} onClose={toggleModal}/>
            <nav className="h-[80px] flex sm:hidden justify-between items-center px-6 shadow-sm fixed top-0 left-0 z-[20] bg-white w-full">
            <div className="w-[100px] h-[50px]">
            <Image
            src="/assets/images/logo/logo.png"
            alt="mont bouet"
            className="h-full object-cover"
            width={100}
            height={50}
          />
            </div>
            <button onClick={toggleModal} className="p-3 rounded-[8px] shadow-sm bg-gray-50 text-mainColor"><IoFilterSharp/></button>
        </nav>
        </>
    );
}

export default NavBarFiltre;