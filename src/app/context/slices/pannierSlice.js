"use client"
import { getIndex } from "@/app/utils/utils";
import { createSlice } from "@reduxjs/toolkit";

export const pannierSlice = createSlice({
    name : "pannier",
    initialState : [],
    reducers : {
        addArticle : (state,action) =>{
            if(state.every(item => item.id != action.payload.id)){
                state.push(action.payload);
            }
        },
        clearPannier : (state,action) =>{
            return [];
        },
        deleteArticle : (state,action) =>{
            state.splice(getIndex(action.payload.id,state),1);
            return state;
        },
        setQuantity : (state,action) =>{
            if (action.payload.article.quantity + action.payload.size > 0) {
                state[getIndex(action.payload.article.id,state)].quantity += 1;
            }
            return state;
        }
    }
})
export const {addArticle,clearPannier,deleteArticle,setQuantity} = pannierSlice.actions;