import Link from "next/link";

import { FaRegUser } from "react-icons/fa";
  import { BiDockTop } from "react-icons/bi";
import { CiShoppingBasket } from "react-icons/ci";
import { IconNotif } from "./IconNotif";
import Image from "next/image";
import { useSelector } from "react-redux";

export const NavBar = () => {
  const pannier = useSelector(state => state.pannier);
  let pannierLength = pannier.reduce((a,b) => a + b.quantity,0);
  return (
    <>
      <div className="px-20 border-b h-[80px] w-full hidden sm:flex justify-between items-center">
        <div className="w-[120px] h-[80%] bg-[royalblue]">
          <Link href="/">
          <Image
            src="/assets/images/logo/logo.png"
            width={488}
            height={208}
            alt="mont bouet"
            className="h-full object-cover"
          />
          </Link>
        </div>
        <div className="h-[80%] flex justify-between items-center gap-2 [&>*]:w-[50px] [&>*]:h-[50px] [&>*]:flex [&>*]:justify-center [&>*]:items-center">
          <div className="relative">
            <Link href="/pages/panier">
            {pannierLength ? <IconNotif>{pannierLength}</IconNotif> : null}
            <CiShoppingBasket className="text-2xl"/>
            </Link>
          </div>
          <Link href="/pages/blog" className="relative">
            {/* <IconNotif>20</IconNotif> */}
            <BiDockTop className="text-2xl"/>
          </Link>
          <div className="relative">
            <Link href="/pages/profil">
            {/* <IconNotif>!</IconNotif> */}
            <FaRegUser className="text-2xl"/>
            </Link>
          </div>
        </div>
      </div>
    </>
  );
};
