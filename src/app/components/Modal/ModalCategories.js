import { Categories } from "../Categories/Categories";

const ModalCategories = ({active = true,onClose}) => {
    return (
        <div className={`fixed ${active ? "translate-y-[0] opacity-1" : "translate-y-[100%] opacity-0"} transition-all duration-800 left-0 top-0 z-[60] h-[100vh] w-full bg-[rgba(0,0,0,0.3)] sm:hidden grid grid-rows-[70px,1fr]`}>
            <div className="flex justify-end items-center px-6">
                <button onClick={onClose} className="text-black bg-white px-3 rounded-[20px] text-[20px]">x</button>
            </div>
            <div className="p-3">
                <div className="bg-white rounded shadow-xl">
                <Categories/>
                </div>
            </div>
        </div>
    );
}

export default ModalCategories;