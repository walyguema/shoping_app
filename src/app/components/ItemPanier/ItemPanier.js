"use client";
import { deleteArticle, setQuantity } from "@/app/context/slices/pannierSlice";
import Image from "next/image";
import { useState } from "react";
import { useDispatch } from "react-redux";

const ItemPanier = ({article}) => {
  const dispatch = useDispatch();
  const addQuantity = (size) => {
    dispatch(setQuantity({size : size,article : article}));
  };
  const removeArticle = () =>{
    dispatch(deleteArticle(article));
  }
  return (
    <div className="grid gap-3 grid-rows-2 rounded-[5px] grid-cols-[120px,1fr,1fr] px-2 py-2 shadow-[0px_2px_3px_2px_rgba(0,0,0,0.04)] bg-white">
      <div className="relative row-span-2">
        <Image
          className="w-full h-full absolute object-contain"
          src={article.images[0]}
          width={200}
          height={200}
          alt={article.name}
        />
      </div>
      <div className="flex flex-col col-span-2 gap-1">
        <p className="font-sans text-[25px]">{article.name}</p>
        <div className="flex gap-2">
          <p className="text-lg">{article.prix.toLocaleString()} FCFA</p>
          <p className="text-lg line-through text-gray-300">{article.promoPrix.toLocaleString()} FCFA</p>
        </div>
      </div>
      <div className="flex items-end">
        <div className="grid grid-cols-[35px,1fr,35px] h-[35px] w-[100px]">
          <button
            className="bg-blue-500 text-white"
            onClick={() => {
              addQuantity(-1);
            }}
          >
            -
          </button>
          <p className="flex justify-center items-center bg-[rgba(0,0,0,0.03)]">
            {article.quantity}
          </p>
          <button
            className="bg-blue-500 text-white"
            onClick={() => {
              addQuantity(1);
            }}
          >
            +
          </button>
        </div>
      </div>
      <div className="flex justify-end items-end">
        <button className="px-3 py-1 rounded bg-red-700 text-white" onClick={removeArticle}>
          Supprimer
        </button>
      </div>
    </div>
  );
};

export default ItemPanier;
