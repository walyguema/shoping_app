export const categories = [
    {
        categorie_name : "Hommes",
        sous_categories : [
            "Chessures",
            "Chemises",
            "Vestes",
            "Montres"
        ],
        size : 200
    },
    {
        categorie_name : "Femmes",
        sous_categories : [
            "Tallons",
            "Maillos",
            "Tenues de soiree",
        ],
        size : 109
    },
    {
        categorie_name : "Electronic",
        sous_categories : [
            "Televisions",
            "Android",
            "Iphone"
        ],
        size : 80
    }
];
export function randomId(){
    return new Date().getTime() + Math.floor(Math.random() * 1000);
}
/**
 * 
 * @param {number} id 
 * @param {[{id : number}]} array 
 */
export function getIndex(id,array){
    for(let i = 0; i < array.length; i++){
        let aId = array[i].id;
        if(id === aId){
            return i;
        }
    }
}
/**
 * 
 * @param {number} id 
 * @param {[{id : number}]} array 
 */
export function includes(id,array){
    for(let i = 0; i < array.length; i++){
        let aId = array[i].id;
        if(id === aId){
            return true;
        }
    }
    return false;
}
