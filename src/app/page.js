"use client"
import { useSelector } from "react-redux";
import { Article } from "./components/Article/Article";
import { Categories } from "./components/Categories/Categories";
import { datas } from "./utils/data";
import {motion} from "framer-motion";
import { forwardRef } from "react";

const MainVariant = {
  initial : {opacity : 1},
  animate : {transition: {staggerChildren: .3}}
}
const BoxVariant = {
  initial : {y : 50,opacity : 0},
  animate : {y : 0,opacity : 1}
}
export default function Home() {
  const data = useSelector(state => state.pannier);
  return (
    <>
      <div className="grid grid-cols-1 lg:grid-cols-[300px,1fr] gap-3 sm:px-10 min-h-[200px]">
        <aside className="hidden lg:block">
          <Categories/>
        </aside>
        <motion.main variants={MainVariant} initial={"initial"} animate={"animate"} className="flex justify-center items-center gap-2 flex-wrap py-5">
        {datas.map((item,i,_) => (
          <Article key={i} article={item} variants={BoxVariant}/>
        ))}
        </motion.main>
      </div>
      
    </>
  )
}