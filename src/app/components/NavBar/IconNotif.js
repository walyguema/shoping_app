export const IconNotif = ({children}) =>{
    return (
        <>
            <span className="absolute top-0 right-0 h-[19px] px-[7px] bg-red-400 rounded-[20%] text-[12px] text-white select-none">{children}</span>
        </>
    )
}