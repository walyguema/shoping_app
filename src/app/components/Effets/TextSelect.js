/**
 * 
 * @param {{options : {color : string,background : string},children : HTMLElement}} param0 
 * @returns 
 */
export const TextSelect = ({options,children}) =>{
    return (
        <>
        <span className={`px-2 inline-block`} style={{background : options?.background ?? "rgba(0,0,0,0.3)",color : options?.color}}>
            {children}
        </span>
        </>
    )
}