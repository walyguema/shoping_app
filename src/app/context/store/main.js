"use client"
import { configureStore } from "@reduxjs/toolkit";
import { rootReducer } from "../rootReducer/rootReducer";

export const mainStore = configureStore({
    reducer : rootReducer
})