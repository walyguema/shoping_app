export const IconSocial = ({children}) =>{
    return (
        <>
            <div className="w-[30px] h-[30px] p-[5px] bg-gray-200 flex justify-center items-center text-gray-700 rounded">
            {children}
            </div>
        </>
    )
}