const { combineReducers } = require("@reduxjs/toolkit");
const { pannierSlice } = require("../slices/pannierSlice");

export const rootReducer = combineReducers({
    pannier : pannierSlice.reducer
})