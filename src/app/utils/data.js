import { randomId } from "./utils";

export const datas = [
    {
        id : randomId(),
        name : "Chessure ADIDAS",
        description : "Nouvelle paire de chessure",
        prix : 18500,
        promoPrix : 12000,
        images : ["/assets/images/ab.jpg","/assets/images/ab.jpg","/assets/images/ab.jpg","/assets/images/ab.jpg"],
        reduction : -15,
        date_publication : "2023-12-05T15:00:00",
        quantity : 1
    },
    {
        id : randomId(),
        name : "ADIDAS",
        description : "Nouvelle paire de chessure",
        prix : 18500,
        promoPrix : 12000,
        images : ["/assets/images/ab.jpg","/assets/images/ab.jpg","/assets/images/ab.jpg","/assets/images/ab.jpg"],
        reduction : -15,
        date_publication : "2023-12-05T15:00:00",
        quantity : 1
    },
    {
        id : randomId(),
        name : "NIKE",
        description : "Nouvelle paire de chessure",
        prix : 18500,
        promoPrix : 12000,
        images : ["/assets/images/ab.jpg","/assets/images/ab.jpg","/assets/images/ab.jpg","/assets/images/ab.jpg"],
        reduction : -15,
        date_publication : "2023-12-05T15:00:00",
        quantity : 1
    },
    {
        id : randomId(),
        name : "Chessure",
        description : "Nouvelle paire de chessure",
        prix : 18500,
        promoPrix : 12000,
        images : ["/assets/images/ab.jpg","/assets/images/ab.jpg","/assets/images/ab.jpg","/assets/images/ab.jpg"],
        reduction : -15,
        date_publication : "2023-12-05T15:00:00",
        quantity : 1
    },
    {
        id : randomId(),
        name : "Chessure",
        description : "Nouvelle paire de chessure",
        prix : 18500,
        promoPrix : 12000,
        images : ["/assets/images/ab.jpg","/assets/images/ab.jpg","/assets/images/ab.jpg","/assets/images/ab.jpg"],
        reduction : -15,
        date_publication : "2023-12-05T15:00:00",
        quantity : 1
    },
    {
        id : randomId(),
        name : "Auth",
        description : "Nouvelle paire de chessure",
        prix : 18500,
        promoPrix : 12000,
        images : ["/assets/images/ab.jpg","/assets/images/ab.jpg","/assets/images/ab.jpg","/assets/images/ab.jpg"],
        reduction : -15,
        date_publication : "2023-12-05T15:00:00",
        quantity : 1
    }
];