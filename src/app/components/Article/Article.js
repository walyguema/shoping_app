"use client";
import Image from "next/image";
import { Bande } from "./Bande";
import { CiShoppingBasket } from "react-icons/ci";
import { useDispatch, useSelector } from "react-redux";
import { addArticle } from "@/app/context/slices/pannierSlice";
import {motion} from 'framer-motion'
import { useState } from "react";
import { includes } from "@/app/utils/utils";

const buttonVariant = {
    init : {
        backgroundColor : "royalblue"
    },
    end : {backgroundColor : "green"}
}

export const Article = ({article,variants}) =>{
    const [isToPanier,setIsToPanier] = useState(false);
    const panier = useSelector(state => state.pannier);
    const dispatch = useDispatch();
    const fillPannier = () =>{
        dispatch(addArticle(article));
    }
    return (
        <>
        <motion.div variants={variants} className="w-full max-w-[350px] md:w-[300px] pb-4 min-h-[300px] shadow rounded">
            <div className="h-[280px] w-full grid grid-rows-[1fr,30px]">
                <div className="relative overflow-hidden flex justify-center items-center">
                    <Bande/>
                    <Image src={article.images[0]} alt={article.name} width={300} height={300} className="absolute w-[100%] h-[90%] object-contain"/>
                </div>
            </div>
            <div className="px-2 py-3">
                <p>{article.name}</p>
                <p className="flex gap-3"><span className="font-bold text-[#333]">{article.promoPrix.toLocaleString()} FCFA</span><span className="line-through font-semibold text-[#555]">{article.prix.toLocaleString()} FCFA</span></p>
            </div>
            <div className="[&>button]:bg-mainColor [&>button]:rounded-[40px] [&>button]:h-[40px] px-2 pb-2">
                <motion.button variants={buttonVariant} animate={includes(article.id,panier) ? "end" : "init"} onClick={fillPannier} className="flex flex-row-reverse text-[15px] justify-center items-center w-full gap-2 text-white">{includes(article.id,panier) ? "" : "Ajouter au panier"}<CiShoppingBasket className="text-2xl"/></motion.button>
            </div>
        </motion.div>
        </>
    )
}
//