"use client"
import { categories } from '@/app/utils/utils';
import { FaRegSmile } from "react-icons/fa";
import { CgGirl } from "react-icons/cg";
import { MdElectricalServices } from "react-icons/md";
import {motion} from "framer-motion";
import Link from 'next/link';
import { useState } from "react"

const mainVariant = {
    init : {height : "auto"},
    end : {height : "auto",transition: {staggerChildren: .1}}
}
const liVariante = {
    init : {position : "absolute",opacity : 0,zIndex : -100,y : 10,display : "none"},
    end : {position : "relative",opacity : 1,zIndex : 1,y : 0,display : "block"}
}

export const Categories = () => {
    const [categorie, setCategorie] = useState(1);
    const icons = {
        Hommes: <FaRegSmile />,
        Femmes: <CgGirl />,
        Electronic: <MdElectricalServices />
    }
    return (
        <>
            <div className='h-full w-full border-r'>
                <div className='p-4'>
                    <h3 className='font-bold mb-4 text-blue-900'>CATEGORIES</h3>
                    <div>
                        {categories.map((item, i, _) => {
                            return (
                                <div key={i}>
                                    <button onClick={() => {setCategorie(i + 1)}} className='flex relative justify-between gap-2 p-2 border-b w-full'><span className='flex gap-3'>{icons[item.categorie_name]} {item.categorie_name}</span> <span>{item.size}</span></button>
                                    <motion.ul initial={"init"} animate={categorie == i + 1 ? "end" : "init"} variants={mainVariant} className={`bg-gray-100 relative`}>
                                    {item.sous_categories.map((cat, k, _) => {
                                        return (
                                                <motion.div variants={liVariante} key={k}>
                                                <Link href="#">
                                                    <li className='p-2 flex justify-between'><span>{cat}</span></li>
                                                </Link>
                                                </motion.div>
                                        )
                                    })}
                                    </motion.ul>
                                </div>
                            )
                        })}
                    </div>
                </div>
            </div>
        </>
    )
}