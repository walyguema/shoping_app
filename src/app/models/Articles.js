export class Articles{
    constructor ({name,description,prix,promoPrix,reduction,date_publication,images}){
        this.state = 0;
        this.name = name;
        this.description = description;
        this.prix = prix;
        this.promoPrix = promoPrix;
        this.reduction = reduction;
        this.date_publication = date_publication;
        this.images = images;
    }
}