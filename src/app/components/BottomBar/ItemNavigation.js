"use client";
import Link from "next/link";
import { usePathname } from "next/navigation";

const ItemNavigation = ({ icon, texte, link = "#", notif = null }) => {
  const route = usePathname();
  return (
    <Link href={link}>
      <div className="flex justify-center items-center flex-col relative">
        <div className={`${route == link ? "text-mainColor" : "text-gray-icon"} relative`}>
        <p className="absolute right-[-20px] top-[-10px]">
        {notif ?? null}
        </p>
          {icon}
        </div>
        {/* <p
          className={`${
            route == link ? "text-mainColor" : "text-gray-icon"
          } text-[14px]`}
        >
          {texte}
        </p> */}
      </div>
    </Link>
  );
};

export default ItemNavigation;
