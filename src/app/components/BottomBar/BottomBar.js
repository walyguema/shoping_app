
  import { FaRegUser } from "react-icons/fa";
  import { BiDockTop } from "react-icons/bi";
import ItemNavigation from "./ItemNavigation";
import { CiShoppingBasket } from "react-icons/ci";
import { BsShop } from "react-icons/bs";
import { IconNotif } from "../NavBar/IconNotif";
import { useSelector } from "react-redux";

const BottomBar = () => {
    const pannier = useSelector(state => state.pannier);
    let pannierLength = pannier.reduce((a,b) => a + b.quantity,0);
    return (
        <div className="block sm:hidden w-full p-2 fixed bottom-0 left-0 z-50">
            <div className="w-full max-w-[430px] m-auto h-full grid grid-cols-4 bg-white py-5 rounded-[4px] shadow-[0px_0px_3px_2px_rgba(0,0,0,0.1)]">
               <ItemNavigation icon={<BsShop className="text-2xl"/>} texte="Accueil" link="/"/>
               <ItemNavigation notif={pannierLength ? <IconNotif>{pannierLength}</IconNotif> : null} icon={<CiShoppingBasket className="text-2xl"/>} link="/pages/panier" texte="Pannier"/>
               <ItemNavigation icon={<BiDockTop className="text-2xl"/>} link="/pages/blog" texte="Blog"/>
               <ItemNavigation icon={<FaRegUser className="text-2xl"/>} texte="Profil" link="/pages/profil"/>
            </div>
        </div>
    );
}

export default BottomBar;