"use client"
import ItemPanier from "@/app/components/ItemPanier/ItemPanier";
import ButtonCommande from "@/app/components/buttonCommande/ButtonCommande";
import { datas } from "@/app/utils/data";
import { useState } from "react";
import { useSelector } from "react-redux";

const Pannier = () => {
    const pannier = useSelector(state => state.pannier);
    return (
        <>
            <div className="px-3 py-5 flex gap-2 flex-wrap justify-center relative">
               {pannier.map((item,i,_) =>(
                <ItemPanier key={i} article={item}/>
               ))}
               <ButtonCommande/>
            </div>
        </>
    );
}

export default Pannier;