import Link from "next/link";
import { IconSocial } from "./IconSocial"
import { FaFacebookF } from "react-icons/fa";
export const TopBar = () =>{
    return (
        <>
            <div className="h-[60px] w-full border-b flex justify-between items-center px-8 sm:px-20">
                <div className="flex gap-3">
                    <IconSocial><FaFacebookF/></IconSocial>
                    <IconSocial><FaFacebookF/></IconSocial>
                    <IconSocial><FaFacebookF/></IconSocial>
                </div>
                <Link href="tel:24174445789" className="text-gray-600 font-bold underline">+241 74445789</Link>
            </div>
        </>
    )
}