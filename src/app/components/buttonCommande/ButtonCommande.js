import { CiShoppingBasket } from "react-icons/ci";
const ButtonCommande = () => {
    return (
        <button className="fixed flex gap-2 right-[20px] bottom-[80px] rounded-[99px] py-3 px-6 shadow-lg bg-green-600 text-white">
        <CiShoppingBasket/>
            <p>Envoyer la commande</p>
        </button>
    );
}

export default ButtonCommande;