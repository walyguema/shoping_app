"use client";
import BottomBar from "./components/BottomBar/BottomBar";
import { NavBar } from "./components/NavBar/NavBar";
import NavBarFiltre from "./components/NavBarFiltre/NavBarFiltre";
import { TopBar } from "./components/TopBar/TopBar";
import "./globals.scss";
import MainProvider from "./context/providers/MainProvider";

export default function RootLayout({ children }) {
  return (
    <>
      <html lang="fr">
      <head>
        <title>Home</title>
      </head>
        <body className="pb-[130px] pt-[100px] sm:pt-0">
          {/* <TopBar /> */}
          <MainProvider>
            <NavBarFiltre />
            <NavBar />
            {children}
            <BottomBar />
          </MainProvider>
        </body>
      </html>
    </>
  );
}
