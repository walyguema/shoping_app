const Profil = () => {
    return (
        <div className="px-3 h-[60vh] flex justify-center items-center">
            <div className="bg-white max-w-[400px] px-3 py-10 shadow-lg rounded-[10px] w-full">
                <h2 className="text-center my-5 text-3xl uppercase">Vous nu&apos;etes pas connecte</h2>
                <button className="p-3 text-center w-full bg-red-500 rounded-[8px] mt-7 text-white">Cree un compte</button>
            </div>
        </div>
    );
}

export default Profil;