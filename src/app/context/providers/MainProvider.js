"use client"
import { Provider } from "react-redux";
import { mainStore } from "../store/main";

const MainProvider = ({children}) => {
    return (
        <Provider store={mainStore}>
        {children}
        </Provider>
    );
}

export default MainProvider;